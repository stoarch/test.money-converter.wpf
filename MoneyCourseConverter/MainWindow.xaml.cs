﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Xml;

namespace MoneyCourseConverter
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        List<Currency> currencyList;
        Dictionary<String, ExchangeRate> exchangeRates;
        ExchangeRate currentExchangeRate;
        Conversion conversion;

        public MainWindow()
        {
            InitializeComponent();

            currencyList = new List<Currency>();
            FillCurrencyList();

            sourceComboBox.DataContext = currencyList;
            resultComboBox.DataContext = currencyList;

            exchangeRates = new Dictionary<string, ExchangeRate>();
            FillExchangeRates();

            currentExchangeRate = FindExchangeRate(currencyList[0], currencyList[1]);
            resultExchangeTextBlock.DataContext = currentExchangeRate;

            conversion = new Conversion();
            conversion.Source = 0.0M;
            conversion.Result = 0.0M;

            sourceTextBox.DataContext = conversion;
            resultTextBlock.DataContext = conversion;
        }

        private ExchangeRate FindExchangeRate(Currency currency1, Currency currency2)
        {
            return exchangeRates[MakeExchangeKey(currency1.Name, currency2.Name)];
        }

        private void FillExchangeRates()
        {
            var rnd = new Random();
            foreach (var i in currencyList)
            {
                foreach( var j in currencyList )
                {
                    exchangeRates.Add(MakeExchangeKey(i.Name, j.Name), MakeExchangeRate(i, j, 1.0M ));
                }
            }
        }

        private ExchangeRate MakeExchangeRate(Currency source, Currency result, Decimal v)
        {
            return new ExchangeRate()
            {
                SourceCurrency = source,
                ResultCurrency = result,
                Rate = v
            };
        }

        private string MakeExchangeKey(string name1, string name2)
        {
            return String.Format("{0}/{1}", name1, name2);
        }

        private void FillCurrencyList()
        {
            currencyList.Add(new Currency() { Color = "Red", IconImageSource = "dollar.png", Name = "USD" });
            currencyList.Add(new Currency() { Color = "Blue", IconImageSource = "euro.png", Name = "EUR" });
            currencyList.Add(new Currency() { Color = "Black", IconImageSource = "Rouble-256.png", Name = "RUB" });
        }

        private void convertButton_Click(object sender, RoutedEventArgs e)
        {
            ConvertCurrentValue();
        }

        private void ConvertCurrentValue()
        {
            conversion.Result = decimal.Round( conversion.Source * currentExchangeRate.Rate, 3, MidpointRounding.AwayFromZero );
        }

        private void refreshRatesButton_Click(object sender, RoutedEventArgs e)
        {
            RefreshExchangeRates();
        }

        private async void RefreshExchangeRates()
        {
            refreshRatesWait.Spin = true;
            refreshRatesButton.IsEnabled = false;
            await LoadExchangeRates();
            refreshRatesWait.Spin = false;
            refreshRatesButton.IsEnabled = true;
        }

        

        private Task<List<ExchangeRate>> LoadExchangeRates()
        {
            var newExchangeRates = new List<ExchangeRate>();
            return Task.Run(() =>
            {
                //string url = "http://finance.yahoo.com/webservice/" +
                //        "v1/symbols/allcurrencies/quote?format=xml";
                string url = "http://query.yahooapis.com/v1/public/yql?q=select * from yahoo.finance.xchange where pair in (";
                foreach (var er in exchangeRates)
                {
                    var sourceName = er.Value.SourceCurrency.Name;
                    var resultName = er.Value.ResultCurrency.Name;
                    if (sourceName == resultName)
                        continue;

                    url += "\"" + sourceName + resultName + "\",";
                }

                url = url.Remove(url.Length - 1);
                url += ")&env=store://datatables.org/alltableswithkeys";

                try
                {
                    // Load the data.
                    XmlDocument doc = new XmlDocument();
                    doc.Load(url);

                    // Process the resource nodes.
                    XmlNode root = doc.DocumentElement;
                    string xquery = "descendant::rate";
                    foreach (XmlNode node in root.SelectNodes(xquery))
                    {
                        const string name_query =
                            "descendant::Name";
                        const string rate_query = 
                            "descendant::Rate";
                        string name =
                            node.SelectSingleNode(name_query).InnerText;
                        string rate =
                            node.SelectSingleNode(rate_query).InnerText;

                        if( exchangeRates.ContainsKey(name) )
                            exchangeRates[name].Rate = decimal.Round( decimal.Parse(rate), 2, MidpointRounding.AwayFromZero );
                    }
                }
                catch (Exception ex)
                {
                    errorsTextBlock.Text = ex.Message;
                }
                Thread.Sleep(1000);
                return newExchangeRates;
            });
        }

        private void sourceComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            UpdateCurrentExchangeRate();
        }

        private void UpdateCurrentExchangeRate()
        {
            if (sourceComboBox.SelectedIndex < 0)
                return;
            if (resultComboBox.SelectedIndex < 0)
                return;

            currentExchangeRate.Rate = FindExchangeRate(currencyList[sourceComboBox.SelectedIndex], currencyList[resultComboBox.SelectedIndex]).Rate;

        }

        private void resultComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            UpdateCurrentExchangeRate();
        }

    }
}
