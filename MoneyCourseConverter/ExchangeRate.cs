﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MoneyCourseConverter
{
    public class ExchangeRate : INotifyPropertyChanged
    {
        public Currency SourceCurrency { get; set; }

        private Decimal rate;
        public Decimal Rate
        {
            get { return rate; }
            set
            {
                rate = value;
                if( PropertyChanged != null )
                    PropertyChanged(this, new PropertyChangedEventArgs("Rate"));
            }
        }
    
        public Currency ResultCurrency { get; set; }

        public event PropertyChangedEventHandler PropertyChanged;
    }
}
