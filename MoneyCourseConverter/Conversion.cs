﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MoneyCourseConverter
{
    class Conversion : INotifyPropertyChanged
    {
        private Decimal source;
        private Decimal result;

        public Decimal Source
        {
            get { return source; }
            set
            {
                source = value;
                NotifyProperty("Source");
            }
        }

        private void NotifyProperty(string fieldName)
        {
            if( PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(fieldName));
            }
        }

        public Decimal Result
        {
            get { return result; }
            set
            {
                result = value;
                NotifyProperty("Result");
            }

        }

        public event PropertyChangedEventHandler PropertyChanged;
    }
}
