﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MoneyCourseConverter
{
    public class Currency
    {
        public String Name { get; set; }
        public String IconImageSource { get; set; }
        public String Color { get; set; }
    }
}
